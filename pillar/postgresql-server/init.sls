postgresql-server:
  local-net: 172.16.4.0/24
  shared_buffers: 128MB
  work_mem: 5MB
  maintenance_work_mem: 4MB
  effective_cache_size: 128MB
