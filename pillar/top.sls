base:
  'roles:redis-server':
    - match: grain
    - gnuviechadmin.redis
{%- for role in ('database', 'queues', 'gva', 'gvaldap', 'gvafile', 'gvamysql', 'gvapgsql', 'gvaweb') %}
  'roles:gnuviechadmin.{{ role }}':
    - match: grain
    - gnuviechadmin.{{ role }}
{% endfor %}
{% for role in ('fileserver', 'ldapserver', 'ldapclient', 'postgresql-server', 'webserver') %}
  'roles:{{ role }}':
    - match: grain
    - {{ role }}
{% endfor %}
