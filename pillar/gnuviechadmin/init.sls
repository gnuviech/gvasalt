include:
  - gnuviechadmin.redis

gnuviechadmin:
  adminname: Gnuviech Admin
  adminemail: admin@gnuviech-server.de
  deploymenttype: local
  domainname: localhost
  ldap_base_dn: dc=gva,dc=local
  ldap_base_dn_groups: ou=groups,dc=gva,dc=local
  ldap_base_dn_users: ou=groups,dc=gva,dc=local
  ldap_domain: gva.local
  ldap_url: ldap://ldap
  mailfrom: admin@gnuviech-server.de
  minosgid: 10000
  minosuid: 10000
  osuserdefaultshell: /usr/bin/rssh
  osuserhomedirbase: /home
  osuserprefix: usr
  sitename: Gnuviech Customer Self Service
  uploadserver: file
  machines:
    salt:
      ip: 172.16.4.10
    mq:
      ip: 172.16.4.20
    syslog:
      ip: 172.16.4.30
    pgsql:
      ip: 172.16.4.40
      names:
        - pgsql
        - gvapgsql
    dns:
      ip: 172.16.4.50
    ldap:
      ip: 172.16.4.60
      names:
        - ldap
        - gvaldap
    file:
      ip: 172.16.4.70
      names:
        - file
        - gvafile
    mail:
      ip: 172.16.4.80
    mysql:
      ip: 172.16.4.90
      names:
        - mysql
        - gvamysql
    web:
      ip: 172.16.4.100
      names:
        - web
        - gvaweb
    service:
      ip: 172.16.4.110
      names:
        - service
        - gva
