include:
  - gnuviechadmin
  - gnuviechadmin.queues.common
  - gnuviechadmin.queues.gvaldap

gnuviechadmin:
  appname: gvaldap
  gvaldap:
    admin_email: jan@dittberner.info
    admin_name: Jan Dittberner
    allowed_hosts: 127.0.0.1,gvaldap.local,localhost,ldap
    amqp_user: ldap
    celery_module: ldaptasks
    django_secret_key: IyOiTDt2DMo4gBVTwZ+E2p+mI1S/rNzZVIFlSr6TpgtxtsJODOVWHaxgVW3FqGZVaFU=
    fullname: LDAP
    git_branch: main
    git_url: https://git.dittberner.info/gnuviech/gvaldap.git
    ldap_admin_password: NnVnGoWBVw6BKb9DhTwHAz0ICrdiDy+HL1A6F2Rz
    ldap_admin_user: ldapadmin
    ldap_groups_ou: groups
    ldap_users_ou: users
    server_email: gvaldap@gnuviech-server.de
