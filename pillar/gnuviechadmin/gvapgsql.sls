include:
  - gnuviechadmin
  - gnuviechadmin.queues.common
  - gnuviechadmin.queues.gvapgsql

gnuviechadmin:
  appname: gvapgsql
  gvapgsql:
    amqp_user: pgsql
    celery_module: pgsqltasks
    fullname: PostgreSQL Server
    git_branch: main
    git_url: https://git.dittberner.info/gnuviech/gvapgsql.git
    pgsql_admin_password: AAv6d1t9p/vtX/kVorim2MJROQfQPWJoZP3mzyMW
    pgsql_admin_user: gvapgsql
