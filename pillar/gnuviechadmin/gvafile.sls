include:
  - gnuviechadmin
  - gnuviechadmin.queues.common
  - gnuviechadmin.queues.gvafile

gnuviechadmin:
  appname: gvafile
  gvafile:
    amqp_user: file
    celery_module: fileservertasks
    fullname: File Server
    git_branch: main
    git_url: https://git.dittberner.info/gnuviech/gvafile.git
    mail_directory: /home/mail
    sftp_authkeys_directory: /srv/sftp/authorized_keys
    sftp_chroot: /srv/sftp
    sftp_group: sftponly
    web_directory: /home/www
