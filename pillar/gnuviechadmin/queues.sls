include:
  - gnuviechadmin.queues.common
  - gnuviechadmin.queues.gvaldap
  - gnuviechadmin.queues.gvafile
  - gnuviechadmin.queues.cli
  - gnuviechadmin.queues.gva
  - gnuviechadmin.queues.gvamysql
  - gnuviechadmin.queues.gvapgsql
  - gnuviechadmin.queues.gvaweb

gnuviechadmin:
  queues:
    users:
      admin:
        password: MmE3Iwylj8Sgy46Z
        perms:
            '/gnuviechadmin':
              - '.*'
              - '.*'
              - '.*'
        tags:
          - administrator
      cli:
        perms:
            '/gnuviechadmin':
              - '.*'
              - '.*'
              - '.*'
        tags:
      file:
        perms:
            '/gnuviechadmin':
              - '.*'
              - '.*'
              - '.*'
        tags:
      gva:
        perms:
            '/gnuviechadmin':
              - '.*'
              - '.*'
              - '.*'
        tags:
      ldap:
        perms:
            '/gnuviechadmin':
              - '.*'
              - '.*'
              - '.*'
        tags:
      mysql:
        perms:
            '/gnuviechadmin':
              - '.*'
              - '.*'
              - '.*'
        tags:
      pgsql:
        perms:
            '/gnuviechadmin':
              - '.*'
              - '.*'
              - '.*'
        tags:
      quotajob:
        perms:
            '/gnuviechadmin':
              - '^quotatool$'
              - '^quotatool$'
              - '^quotatool|amq.default$'
        tags:
      web:
        perms:
            '/gnuviechadmin':
              - '.*'
              - '.*'
              - '.*'
        tags:
