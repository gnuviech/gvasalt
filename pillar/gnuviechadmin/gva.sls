include:
  - gnuviechadmin
  - gnuviechadmin.database
  - gnuviechadmin.queues.common
  - gnuviechadmin.queues.gva

gnuviechadmin:
  appname: gva
  database:
    host: pgsql
  gva:
    django_secret_key: "yBnbG4azhNaTxIW0/Rv2dEij9PcVU1KVR//1bR6LujmLBnZJw8OOrEi2dIqz3pyOdG8="
    fullname: Self Service Web Interface
    git_branch: main
    git_url: https://git.dittberner.info/gnuviech/gva.git
    url_mysql_admin: https://phpmyadmin.gva.local/
    url_pgsql_admin: https://phppgadmin.gva.local/
    url_webmail: https://webmail.gva.local/
