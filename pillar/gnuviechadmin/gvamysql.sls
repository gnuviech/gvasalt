include:
  - gnuviechadmin
  - gnuviechadmin.queues.common
  - gnuviechadmin.queues.gvamysql

gnuviechadmin:
  appname: gvamysql
  gvamysql:
    amqp_user: mysql
    celery_module: mysqltasks
    fullname: MySQL Server
    git_branch: main
    git_url: https://git.dittberner.info/gnuviech/gvamysql.git
    mysql_admin_password: jSXstgT/AbWofdI2tJWYpQvFX1mtxt4tFMlrYxSA
    mysql_admin_user: gvamysql

mysql.default_file: /etc/mysql/debian.cnf
