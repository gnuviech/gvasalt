include:
  - gnuviechadmin
  - gnuviechadmin.queues.common
  - gnuviechadmin.queues.gvaweb

gnuviechadmin:
  appname: gvaweb
  gvaweb:
    amqp_user: web
    celery_module: webtasks
    fullname: Web
    git_branch: main
    git_url: https://git.dittberner.info/gnuviech/gvaweb.git
