mariadb-server:
  pkg.installed

mysql:
  service.running:
    - enable: true
    - require:
      - pkg: mariadb-server

/etc/mysql/my.cnf:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - source: salt://mariadb-server/my.cnf
    - watch_in:
      - service: mysql
