nginx:
  pkg:
    - installed
  service.running:
    - enable: True
    - require:
      - pkg: nginx

nginx-common:
  pkg.installed

/etc/nginx/nginx.conf:
  file.managed:
    - source: salt://nginx/nginx.conf
    - user: root
    - group: root
    - mode: 0644
    - require:
      - pkg: nginx-common
    - watch_in:
      - service: nginx

{% set nginx_ssl_keydir = salt['pillar.get']('nginx:sslkeydir', '/etc/nginx/ssl/private') %}
{% set nginx_ssl_certdir = salt['pillar.get']('nginx:sslcertdir', '/etc/nginx/ssl/certs') %}

{{ nginx_ssl_certdir }}:
  file.directory:
    - user: root
    - group: root
    - mode: 0755
    - makedirs: True

{{ nginx_ssl_keydir }}:
  file.directory:
    - user: root
    - group: root
    - mode: 0750
    - makedirs: True

generate-dhparam-nginx:
  cmd.run:
    - name: openssl dhparam -out {{ nginx_ssl_keydir }}/dhparams.pem 2048
    - umask: 022
    - runas: root
    - timeout: 300
    - output_loglevel: debug
    - creates: {{ nginx_ssl_keydir }}/dhparams.pem
    - require:
      - file: {{ nginx_ssl_keydir }}
    - require_in:
      - file: /etc/nginx/conf.d/ssl.conf
    - watch_in:
      - service: nginx

/etc/nginx/conf.d/ssl.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - source: salt://nginx/nginx-ssl.conf
    - template: jinja
    - require:
      - pkg: nginx
    - watch_in:
      - service: nginx

/etc/nginx/snippets/security.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - source: salt://nginx/nginx-security.conf
    - require:
      - pkg: nginx
    - watch_in:
      - service: nginx

/etc/nginx/conf.d/logformat.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - source: salt://nginx/nginx-logformat.conf
    - require:
      - pkg: nginx
    - watch_in:
      - service: nginx
