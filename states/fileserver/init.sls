base-dirs:
  file.directory:
    - names:
      - /srv/nfs4
      - /srv/sftp
    - user: root
    - group: root
    - mode: 0755

home-dirs:
  file.directory:
    - names:
      - /home/mail
      - /home/www
    - user: root
    - group: root
    - mode: 0751

nfs4-dirs:
  file.directory:
    - names:
      - /srv/nfs4/web
      - /srv/nfs4/mail
    - user: root
    - group: root
    - mode: 0751
    - require:
      - file: /srv/nfs4

/srv/nfs4/web:
  mount.mounted:
    - device: /home/www
    - fstype: none
    - opts:
      - bind
    - persist: True
    - require:
      - file: nfs4-dirs
      - file: /home/www

/srv/nfs4/mail:
  mount.mounted:
    - device: /home/mail
    - fstype: none
    - opts:
      - bind
    - persist: True
    - require:
      - file: nfs4-dirs
      - file: /home/mail

/srv/sftp/home:
  file.directory:
    - user: root
    - group: root
    - mode: 0751
  mount.mounted:
    - device: /home/www
    - fstype: none
    - opts:
      - bind
    - persist: True
    - require:
      - file: /srv/sftp/home
      - file: /home/www

fileserver-packages:
  pkg.installed:
    - pkgs:
      - nfs-kernel-server
      - rssh
  service.running:
    - name: nfs-kernel-server
    - require:
      - pkg: fileserver-packages
      - mount: /srv/nfs4/mail
      - mount: /srv/nfs4/web

/etc/exports:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - source: salt://fileserver/exports
    - watch_in:
      - service: nfs-kernel-server

{% if 'sftp_group' in pillar %}
/srv/sftp/authorized_keys:
  file.directory:
    - user: root
    - group: root
    - mode: 0701
{% endif %}

sshd:
  pkg.installed:
    - pkgs:
      - openssh-server
      - openssh-blacklist
      - openssh-blacklist-extra
  service.running:
    - name: ssh
    - require:
      - pkg: sshd
{% if 'sftp_group' in pillar %}
      - file: /srv/sftp/authorized_keys
{% endif %}
    - watch:
      - file: /etc/ssh/sshd_config

/etc/ssh/sshd_config:
  file.managed:
    - source: salt://fileserver/sshd_config
    - template: jinja
    - user: root
    - group: root
    - mode: 0644
