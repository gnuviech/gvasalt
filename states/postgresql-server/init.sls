locales-all:
  pkg.installed

postgresql:
  pkg:
    - installed
  service.running:
    - require:
      - pkg: postgresql

/etc/postgresql/11/main/conf.d/custom.conf:
  file.managed:
    - user: postgres
    - group: postgres
    - source: salt://postgresql-server/custom.conf
    - template: jinja
    - mode: 0644
    - require:
      - pkg: postgresql
    - watch_in:
      - service: postgresql

/etc/postgresql/11/main/pg_hba.conf:
  file.append:
    - source: salt://postgresql-server/pg_hba_line.conf
    - template: jinja
    - require:
      - pkg: postgresql
    - watch_in:
      - service: postgresql
