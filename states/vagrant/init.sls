include:
  - vim

/home/vagrant/bin:
  file.directory:
    - user: vagrant
    - group: vagrant
    - mode: 0750

/home/vagrant/.bashrc:
  file.managed:
    - user: vagrant
    - group: vagrant
    - mode: 0644
    - source: salt://vagrant/bashrc

/home/vagrant/.vimrc:
  file.managed:
    - user: vagrant
    - group: vagrant
    - mode: 0644
    - source: salt://vagrant/vimrc
    - require:
      - pkg: vim-nox
