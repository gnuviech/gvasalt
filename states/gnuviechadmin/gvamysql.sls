---
{% set gvaappname = salt['pillar.get']('gnuviechadmin:appname') %}
{% set purpose = "for MySQL/MariaDB configuration management" %}
{% set mysql_admin_user = salt['pillar.get']('gnuviechadmin:{}:mysql_admin_user'.format(gvaappname), 'gvamysql') %}
{% set mysql_admin_password = salt['pillar.get']('gnuviechadmin:{}:mysql_admin_password'.format(gvaappname)) %}
{% from 'gnuviechadmin/gvaapp_macros.sls' import create_celery_worker with context %}
include:
- python.poetry
- mariadb-server

{{ create_celery_worker(gvaappname, purpose) }}

{{ gvaappname }}-dependencies:
  pkg.installed:
    - pkgs:
      - python3-dev
      - libmariadb-dev-compat
    - require_in:
      - cmd: {{ gvaappname }}-requirements

python3-mysqldb:
  pip.installed:
    - name: mysqlclient

gvamysql-mysql-user:
  mysql_user.present:
    - name: {{ mysql_admin_user }}
    - host: '%'
    - password: {{ mysql_admin_password }}
    - require:
      - pip: python3-mysqldb

gvamysql-grants-all-dbs:
  mysql_grants.present:
    - grant: ALL PRIVILEGES
    - database: '*.*'
    - grant_option: True
    - user: {{ mysql_admin_user }}
    - password: {{ mysql_admin_password }}
    - host: '%'
    - require:
      - pip: python3-mysqldb
      - mysql_user: gvamysql-mysql-user
