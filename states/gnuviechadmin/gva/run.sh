#!/bin/sh

set -e
cd "{{ checkout }}/gnuviechadmin"

export DJANGO_SETTINGS_MODULE=gnuviechadmin.settings
export GVA_ADMIN_EMAIL={{ salt['pillar.get']('gnuviechadmin:adminemail', 'admin@example.org') }}
export GVA_ADMIN_NAME={{ salt['pillar.get']('gnuviechadmin:adminname', 'Gnuviech Admin') }}
export GVA_BROKER_URL={{ broker_url }}
export GVA_DOMAIN_NAME={{ salt['pillar.get']('gnuviechadmin:{}:domainname'.format(gvaappname), 'service.localhost') }}
export GVA_LOG_FILE=/var/log/gnuviechadmin/{{ gvaappname }}.log
export GVA_MIN_OS_GID={{ salt['pillar.get']('gnuviechadmin:minosgid', 10000) }}
export GVA_MIN_OS_UID={{ salt['pillar.get']('gnuviechadmin:minosuid', 10000) }}
export GVA_OSUSER_DEFAULT_SHELL={{ salt['pillar.get']('gnuviechadmin:osuserdefaultshell', '/sbin/nologin') }}
export GVA_OSUSER_HOME_BASEPATH={{ salt['pillar.get']('gnuviechadmin:osuserhomedirbase', '/home') }}
export GVA_OSUSER_PREFIX={{ salt['pillar.get']('gnuviechadmin:osuserprefix', 'user') }}
export GVA_OSUSER_UPLOADSERVER={{ salt['pillar.get']('gnuviechadmin:uploadserver') }}
export GVA_PGSQL_DATABASE={{ salt['pillar.get']('gnuviechadmin:database:name') }}
export GVA_PGSQL_HOSTNAME={{ salt['pillar.get']('gnuviechadmin:database:host', 'localhost') }}
export GVA_PGSQL_PASSWORD={{ salt['pillar.get']('gnuviechadmin:database:owner:password') }}
export GVA_PGSQL_PORT={{ salt['pillar.get']('gnuviechadmin:database:port', 5432) }}
export GVA_PGSQL_USER={{ salt['pillar.get']('gnuviechadmin:database:owner:user', gvaappname ) }}
export GVA_RESULTS_REDIS_URL={{ result_url }}
export GVA_SITE_ADMINMAIL={{ salt['pillar.get']('gnuviechadmin:adminemail', 'admin@example.org') }}
export GVA_SITE_NAME={{ salt['pillar.get']('gnuviechadmin:sitename') }}
export GVA_SITE_SECRET={{ salt['pillar.get']('gnuviechadmin:{}:django_secret_key'.format(gvaappname)) }}
export GVA_URL_MYSQL_ADMIN={{ salt['pillar.get']('gnuviechadmin:{}:url_mysql_admin'.format(gvaappname)) }}
export GVA_URL_PGSQL_ADMIN={{ salt['pillar.get']('gnuviechadmin:{}:url_pgsql_admin'.format(gvaappname)) }}
export GVA_URL_WEBMAIL={{ salt['pillar.get']('gnuviechadmin:{}:url_webmail'.format(gvaappname)) }}
export LANG=C.UTF-8
export LC_ALL=C.UTF-8

{{ checkout }}/.venv/bin/python3 manage.py $*
