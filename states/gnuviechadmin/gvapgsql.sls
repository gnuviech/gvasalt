---
{% set gvaappname = salt['pillar.get']('gnuviechadmin:appname') %}
{% set purpose = "for PostgreSQL configuration management" %}
{% from 'gnuviechadmin/gvaapp_macros.sls' import create_celery_worker with context %}
include:
- python.poetry
- postgresql-server

{{ create_celery_worker(gvaappname, purpose) }}

{{ gvaappname }}-dependencies:
  pkg.installed:
    - pkgs:
      - python3-dev
      - libpq-dev
    - require_in:
      - cmd: {{ gvaappname }}-requirements

gvapgsql-pgsql-user:
  postgres_user.present:
    - name: {{ salt['pillar.get']('gnuviechadmin:{}:pgsql_admin_user'.format(gvaappname), 'gvapgsql') }}
    - password: {{ salt['pillar.get']('gnuviechadmin:{}:pgsql_admin_password'.format(gvaappname)) }}
    - user: postgres
    - superuser: True
    - login: True
    - watch_in:
      - service: {{ gvaappname }}-celery-worker
    - require:
      - pkg: postgresql
