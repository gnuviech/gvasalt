---
{% set gvaappname = salt['pillar.get']('gnuviechadmin:appname') %}
{% set purpose = "for LDAP data management" %}
{% from 'gnuviechadmin/gvaapp_macros.sls' import create_celery_worker with context %}
include:
- python.poetry

{{ create_celery_worker(gvaappname, purpose) }}

{{ gvaappname }}-dependencies:
  pkg.installed:
    - pkgs:
      - python3-dev
      - libldap2-dev
      - libsasl2-dev
    - require_in:
      - cmd: {{ gvaappname }}-requirements

base-ldap-objects:
  cmd.script:
    - source: salt://gnuviechadmin/gvaldap/create_base_ldap_objects.sh
    - template: jinja
    - runas: root
    - unless: ldapsearch -Y EXTERNAL -H ldapi:// -b "{{ salt['pillar.get']('gnuviechadmin:ldap_base_dn') }}" "cn={{ salt['pillar.get']('gnuviechadmin:gvaldap:ldap_admin_user') }}" | grep -q numEntries
