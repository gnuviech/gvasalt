---
{% set gvaappname = salt['pillar.get']('gnuviechadmin:appname') %}
{% set app_home = salt['grains.get']('gnuviechadmin:home', '/home/{}'.format(gvaappname)) %}
{% set app_user = salt['grains.get']('gnuviechadmin:user', gvaappname) %}
{% set app_group = salt['grains.get']('gnuviechadmin:group', gvaappname) %}

{% set amqp_user = salt['pillar.get']('gnuviechadmin:{}:amqp_user'.format(gvaappname), gvaappname) -%}
{% set checkout = salt['grains.get']('gnuviechadmin:checkout', '/srv/{}'.format(gvaappname)) -%}
{% set domainname = salt['pillar.get']('gnuviechadmin:{}:domainname'.format(gvaappname), 'service.localhost') %}
{% set update_git = salt['grains.get']('gnuviechadmin:update_git', True) %}
{% set gitrepo = salt['pillar.get']('gnuviechadmin:{}:git_url'.format(gvaappname), 'git:gnuviech/{}.git'.format(gvaappname)) -%}

{% from 'gnuviechadmin/gvaapp_macros.sls' import gvaapp_base with context %}
include:
- base
- python.poetry
- uwsgi.python3

{{ gvaapp_base(gvaappname, 'uwsgi') }}

{{ gvaappname }}-dependencies:
  pkg.installed:
    - pkgs:
      - libpq-dev
    - require_in:
      - cmd: {{ gvaappname }}-requirements
      - service: uwsgi

gettext:
  pkg.installed

{{ checkout }}/run{{ gvaappname }}:
  file.managed:
    - user: {{ app_user }}
    - group: {{ app_group }}
    - mode: 0750
    - source: salt://gnuviechadmin/{{ gvaappname }}/run.sh
    - template: jinja
    - context:
        broker_url: amqp://{{ amqp_user }}:{{ salt['pillar.get']('gnuviechadmin:queues:users:{}:password'.format(amqp_user)) }}@{{ salt['pillar.get']('gnuviechadmin:amqp_host', 'mq') }}/{{ salt['pillar.get']('gnuviechadmin:queues:vhost') }}
        result_url: redis://:{{ salt['pillar.get']('gnuviechadmin:redis_password') }}@{{ salt['pillar.get']('gnuviechadmin:redis_host') }}/0
        gvaappname: {{ gvaappname }}
        checkout: {{ checkout }}
    - require:
      - git: {{ gitrepo }}

{% for command in ['migrate --noinput', 'collectstatic --noinput', 'compilemessages'] %}
{{ gvaappname }}-manage-{{ command }}:
  cmd.wait:
    - name: {{ checkout }}/run{{ gvaappname }} {{ command }}
    - runas: {{ app_user }}
    - cwd: {{ checkout }}/gnuviechadmin
    - watch:
      - cmd: {{ gvaappname }}-requirements
      {%- if update_git %}
      - git: {{ gitrepo }}
      {%- endif %}
{% endfor %}

/etc/uwsgi/apps-available/{{ gvaappname }}.ini:
  file.managed:
    - user: root
    - group: {{ app_group }}
    - mode: 0640
    - source: salt://gnuviechadmin/{{ gvaappname }}/uwsgi.ini
    - template: jinja
    - context:
        gvaappname: {{ gvaappname }}
        broker_url: amqp://{{ amqp_user }}:{{ salt['pillar.get']('gnuviechadmin:queues:users:{}:password'.format(amqp_user)) }}@{{ salt['pillar.get']('gnuviechadmin:amqp_host', 'mq') }}/{{ salt['pillar.get']('gnuviechadmin:queues:vhost') }}
        result_url: redis://:{{ salt['pillar.get']('gnuviechadmin:redis_password') }}@{{ salt['pillar.get']('gnuviechadmin:redis_host') }}/0
        workdir: {{ checkout }}/gnuviechadmin
        venv: {{ checkout }}/.venv
    - require:
      - group: {{ gvaappname }}-group
      - pkg: uwsgi
    - require_in:
      - service: uwsgi
    - watch_in:
      - service: uwsgi

/etc/uwsgi/apps-enabled/{{ gvaappname }}.ini:
  file.symlink:
    - target: /etc/uwsgi/apps-available/{{ gvaappname }}.ini
    - require:
      - file: /etc/uwsgi/apps-available/{{ gvaappname }}.ini
    - require_in:
      - service: uwsgi

{% set letsencrypt = salt['pillar.get']('gnuviechadmin:{}:letsencrypt'.format(gvaappname), False) %}
{% if not letsencrypt %}
python3-cryptography:
  pkg.installed

{% from 'webserver/sslcert.macros.sls' import key_cert with context %}
{{ key_cert(domainname) }}
{% endif %}

/etc/nginx/sites-available/{{ domainname }}:
  file.managed:
    - user: root
    - group: root
    - mode: 0640
    - source: salt://gnuviechadmin/{{ gvaappname }}/app.nginx
    - template: jinja
    - context:
        domainname: {{ domainname }}
        checkout: {{ checkout }}
        letsencrypt: {{ letsencrypt }}
        appname: {{ gvaappname }}
    - require:
      - pkg: nginx
    - watch_in:
      - service: nginx

/etc/nginx/sites-enabled/{{ domainname }}:
  file.symlink:
    - target: /etc/nginx/sites-available/{{ domainname }}
    - require:
      - file: /etc/nginx/sites-available/{{ domainname }}
      - file: /etc/uwsgi/apps-enabled/{{ gvaappname }}.ini
      - service: uwsgi
    - watch_in:
      - service: nginx
