---
{% macro gvaapp_base(gvaappname, servicename) -%}
{% set app_home = salt['grains.get']('gnuviechadmin:home', '/home/{}'.format(gvaappname)) %}
{% set app_user = salt['grains.get']('gnuviechadmin:user', gvaappname) %}
{% set app_group = salt['grains.get']('gnuviechadmin:group', gvaappname) %}

{% set appfullname = 'GNUViech Admin {} User'.format(salt['pillar.get']('gnuviechadmin:{}:fullname'.format(gvaappname))) -%}
{% set update_git = salt['grains.get']('gnuviechadmin:update_git', True) %}
{% set gitrepo = salt['pillar.get']('gnuviechadmin:{}:git_url'.format(gvaappname), 'git:gnuviech/{}.git'.format(gvaappname)) -%}
{% set checkout = salt['grains.get']('gnuviechadmin:checkout', '/srv/{}'.format(gvaappname)) -%}
{% set deployment_key = '{}/.ssh/id_deployment'.format(app_home) -%}

{% for host in salt['pillar.get']('gnuviechadmin:machines', {}) %}
{% if host != salt['grains.get']('host') %}
{{ host }}:
  host.present:
    - ip: {{ salt['pillar.get']('gnuviechadmin:machines:{}:ip'.format(host)) }}
{% if salt['pillar.get']('gnuviechadmin:machines:{}:names'.format(host)) %}
    - names:
{% for machine in salt['pillar.get']('gnuviechadmin:machines:{}:names'.format(host)) %}
      - {{ machine }}
{% endfor %}
{% endif %}
{% endif %}
{% endfor %}

{{ gvaappname }}-group:
  group.present:
    - name: {{ app_group }}

{{ gvaappname }}-user:
  user.present:
    - name: {{ app_user }}
    - home: {{ app_home }}
    - shell: /bin/bash
    - fullname: {{ appfullname }}
    - groups:
      - {{ app_group }}
    - require:
      - group: {{ gvaappname }}-group
  alias.present:
    - target: root

gvabase-dependencies:
  pkg.installed:
    - name: build-essential

/var/log/gnuviechadmin:
  file.directory:
    - user: {{ app_user }}
    - group: {{ app_group }}
    - mode: 0750
    - require:
      - user: {{ gvaappname }}-user
      - group: {{ gvaappname }}-group

{{ checkout }}:
  file.directory:
    - user: {{ app_user }}
    - group: {{ app_group }}
    - mode: 0755
    - require:
      - user: {{ gvaappname }}-user

{% if update_git %}
{{ gitrepo }}:
  git.latest:
    - user: {{ app_user }}
    - target: {{ checkout }}
    - rev: {{ salt['pillar.get']('gnuviechadmin:{}:git_branch'.format(gvaappname), 'production') }}
    - force_clone: true
    - require:
      - file: {{ checkout }}
    - watch_in:
      - cmd: {{ gvaappname }}-requirements
      - service: {{ servicename }}
{% endif %}

{{ gvaappname }}-requirements:
  cmd.run:
    - name: /usr/local/poetry/bin/poetry install
    - runas: {{ app_user }}
    - cwd: {{ checkout }}
    - env:
      - POETRY_VIRTUALENVS_IN_PROJECT: "true"
      - LC_ALL: C.UTF-8
      - LANG: C.UTF-8
    - require:
      - cmd: install_poetry
      {%- if update_git %}
      - git: {{ gitrepo }}
      {%- else %}
      - file: {{ checkout }}
      {%- endif %}
      - pkg: gvabase-dependencies
    - unless: test $(find {{ checkout }}/.venv -type f -cnewer poetry.lock \! -name '*.pyc'|wc -l) -gt 0
    - watch_in:
      - service: {{ servicename }}
{% endmacro %}

{% macro create_celery_worker(gvaappname, purpose) %}
{% set app_home = salt['grains.get']('gnuviechadmin:home', '/home/{}'.format(gvaappname)) %}
{% set app_user = salt['grains.get']('gnuviechadmin:user', gvaappname) %}
{% set app_group = salt['grains.get']('gnuviechadmin:group', gvaappname) %}

{% set checkout = salt['grains.get']('gnuviechadmin:checkout', '/srv/{}'.format(gvaappname)) -%}
{% set gitrepo = salt['pillar.get']('gnuviechadmin:{}:git_url'.format(gvaappname), 'git:gnuviech/{}.git'.format(gvaappname)) -%}
{% set update_git = salt['grains.get']('gnuviechadmin:update_git', True) %}

{% set servicename = "{}-celery-worker".format(gvaappname) %}
{% set amqp_user = salt['pillar.get']('gnuviechadmin:{}:amqp_user'.format(gvaappname)) -%}
{{ gvaapp_base(gvaappname, servicename) }}
/etc/default/{{ gvaappname }}:
  file.managed:
    - user: root
    - group: root
    - mode: 0640
    - source: salt://gnuviechadmin/{{ gvaappname }}/celery-worker.env
    - template: jinja
    - context:
        checkout: {{ checkout }}
        broker_url: amqp://{{ amqp_user }}:{{ salt['pillar.get']('gnuviechadmin:queues:users:{}:password'.format(amqp_user)) }}@{{ salt['pillar.get']('gnuviechadmin:amqp_host', 'mq') }}/{{ salt['pillar.get']('gnuviechadmin:queues:vhost') }}
        result_url: redis://:{{ salt['pillar.get']('gnuviechadmin:redis_password') }}@{{ salt['pillar.get']('gnuviechadmin:redis_host') }}/0
    - watch_in:
      - service: {{ servicename }}

/etc/systemd/system/{{ servicename }}.service:
  file.managed:
    - user: root
    - group: {{ app_group }}
    - mode: 0644
    - source: salt://gnuviechadmin/celery-worker.service
    - template: jinja
    - context:
        checkout: {{ checkout }}
        app_user: {{ app_user }}
        appname: {{ gvaappname }}
        celery_module: {{ salt['pillar.get']('gnuviechadmin:{}:celery_module'.format(gvaappname), gvaappname) }}
        amqpname: {{ amqp_user }}
        description: Gnuviechadmin celery worker {{ purpose|default(gvaappname) }}
    - watch_in:
      - service: {{ servicename }}

{{ servicename }}:
  service.running:
    - enable: True
    - require:
      {%- if update_git %}
      - git: {{ gitrepo }}
      {%- else %}
      - file: {{ checkout }}
      {%- endif %}
      - file: /etc/systemd/system/{{ servicename }}.service
    - watch:
      - cmd: {{ gvaappname }}-requirements
      {%- if update_git %}
      - git: {{ gitrepo }}
      {%- endif %}
{% endmacro %}
