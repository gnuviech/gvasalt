include:
  - postgresql-server

gnuviechadmin-database:
  postgres_user.present:
    - name: {{ salt['pillar.get']('gnuviechadmin:database:owner:user') }}
    - user: postgres
    - password: {{ salt['pillar.get']('gnuviechadmin:database:owner:password') }}
    - login: True
    - createdb: {% if salt['pillar.get']('gnuviechadmin:deploymenttype', 'production') == 'local' %}True
{%- else %}False
{%- endif %}
    - require:
      - service: postgresql
  postgres_database.present:
    - name: {{ salt['pillar.get']('gnuviechadmin:database:name') }}
    - user: postgres
    - owner: {{ salt['pillar.get']('gnuviechadmin:database:owner:user') }}
    - encoding: UTF8
    - template: template0
    - require:
      - service: postgresql
      - postgres_user: {{ salt['pillar.get']('gnuviechadmin:database:owner:user') }}

{% for gnuviechadmin_db_role in salt['pillar.get']('gnuviechadmin:database:users') %}
gnuviechadmin-dbuser-{{ gnuviechadmin_db_role }}:
  postgres_user.present:
    - name: {{ salt['pillar.get']('gnuviechadmin:database:users:%s:user' % gnuviechadmin_db_role) }}
    - password: {{ salt['pillar.get']('gnuviechadmin:database:users:%s:password' % gnuviechadmin_db_role) }}
    - login: True
    - require:
      - service: postgresql
{% endfor %}
