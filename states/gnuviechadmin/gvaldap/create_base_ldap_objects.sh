#!/bin/sh

set -e

{% set base_dn = salt['pillar.get']('gnuviechadmin:ldap_base_dn') %}
{% set ldap_admin_user = salt['pillar.get']('gnuviechadmin:gvaldap:ldap_admin_user') %}
{% set ldap_admin_password = salt['pillar.get']('gnuviechadmin:gvaldap:ldap_admin_password') %}
{% set ldap_groups_ou = salt['pillar.get']('gnuviechadmin:gvaldap:ldap_groups_ou') %}
{% set ldap_users_ou = salt['pillar.get']('gnuviechadmin:gvaldap:ldap_users_ou') %}

# setup password hashing for cleartext input
ldapadd -v -H ldapi:// -Y EXTERNAL -f /etc/ldap/schema/ppolicy.ldif

ldapmodify -v -H ldapi:// -Y EXTERNAL <<EOD
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: ppolicy

dn: olcOverlay=ppolicy,olcDatabase={1}mdb,cn=config
changetype: add
objectClass: olcOverlayConfig
objectClass: olcPPolicyConfig
olcOverlay: ppolicy
olcPPolicyHashClearText: TRUE
EOD

# define ACLs on LDAP tree
ldapmodify -v -H ldapi:// -Y EXTERNAL <<EOD
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to attrs=userPassword,shadowLastChange
  by self write
  by anonymous auth
  by dn="cn={{ ldap_admin_user }},{{ base_dn }}" write
  by * none
olcAccess: {1}to dn.base=""
  by * read
olcAccess: {2}to dn.subtree="ou={{ ldap_users_ou }},{{ base_dn }}"
  by dn="cn={{ ldap_admin_user }},{{ base_dn }}" write
  by * read
olcAccess: {3}to dn.subtree="ou={{ ldap_groups_ou }},{{ base_dn }}"
  by dn="cn={{ ldap_admin_user }},{{ base_dn }}" write
  by * read
olcAccess: {4}to *
  by self write
  by * read
EOD

# add OUs, groups and ldapadmin user
ldapmodify -v -H {{ salt['pillar.get']('gnuviechadmin:ldap_url') }} -x -D "cn=admin,{{ base_dn }}" -w '{{ salt["pillar.get"]("slapd:admin_password") }}' <<EOD
dn: ou={{ ldap_users_ou }},{{ base_dn }}
changetype: add
objectClass: top
objectClass: organizationalUnit
ou: {{ ldap_users_ou }}

dn: ou={{ ldap_groups_ou }},{{ base_dn }}
changetype: add
objectClass: top
objectClass: organizationalUnit
ou: {{ ldap_groups_ou }}

dn: cn=sftponly,ou={{ ldap_groups_ou }},{{ base_dn }}
changetype: add
objectClass: posixGroup
cn: sftponly
gidNumber: 2000
description: SFTP users

dn: cn=wwwusers,ou={{ ldap_groups_ou }},{{ base_dn }}
changetype: add
objectClass: posixGroup
cn: wwwusers
gidNumber: 2001

dn: cn=webserver,ou={{ ldap_groups_ou }},{{ base_dn }}
changetype: add
objectClass: posixGroup
cn: webserver
gidNumber: 2002
memberUid: www-data

dn: cn={{ ldap_admin_user }},{{ base_dn }}
changetype: add
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: {{ ldap_admin_user }}
description: LDAP manager for celery worker
userPassword:: {{ salt['hashutil.base64_b64encode'](ldap_admin_password) }}
EOD
