include:
  - rabbitmq-server

gnuviechadmin-queue-vhost:
  rabbitmq_vhost.present:
    - name: {{ salt['pillar.get']('gnuviechadmin:queues:vhost') }}

gnuviechadmin_test-queue-vhost:
  rabbitmq_vhost.present:
    - name: {{ "%s_test" % salt['pillar.get']('gnuviechadmin:queues:vhost') }}

{% for user in salt['pillar.get']('gnuviechadmin:queues:users') %}
gnuviechadmin-queue-user-{{ user }}:
  rabbitmq_user.present:
    - name: {{ user }}
    - password: {{ salt['pillar.get']('gnuviechadmin:queues:users:%s:password' % user) }}
{% if salt['pillar.get']('gnuviechadmin:queues:users:%s:perms' % user) %}
    - perms:
{% for vhost, perms in salt['pillar.get']('gnuviechadmin:queues:users:%s:perms' % user).items() %}
      - {{ vhost }}:
        - {{ perms[0] }}
        - {{ perms[1] }}
        - {{ perms[2] }}
      - {{ vhost + "_test" }}:
        - {{ perms[0] }}
        - {{ perms[1] }}
        - {{ perms[2] }}
{% endfor %}
{% endif %}
{% if salt['pillar.get']('gnuviechadmin:queues:users:%s:tags' % user) %}
    - tags:
{% for tag in salt['pillar.get']('gnuviechadmin:queues:users:%s:tags' % user) %}
      - {{ tag }}
{% endfor %}
{% endif %}
    - require:
      - rabbitmq_vhost: {{ salt['pillar.get']('gnuviechadmin:queues:vhost') }}
{% endfor %}
