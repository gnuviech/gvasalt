---
{% set gvaappname = salt['pillar.get']('gnuviechadmin:appname') %}
{% set purpose = "for website configuration management" %}
{% from 'gnuviechadmin/gvaapp_macros.sls' import create_celery_worker with context %}
include:
- base
- python.poetry

{{ create_celery_worker(gvaappname, purpose) }}

/etc/sudoers.d/{{ gvaappname }}:
  file.managed:
    - user: root
    - group: root
    - source: salt://gnuviechadmin/{{ gvaappname }}/sudoers
    - template: jinja
    - context:
        app_user: {{ salt['grains.get']('gnuviechadmin:user', gvaappname) }}
    - check_cmd: /usr/sbin/visudo -c -f
    - require:
      - pkg: sudo
