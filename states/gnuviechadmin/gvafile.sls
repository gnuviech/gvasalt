{% set gvaappname = salt['pillar.get']('gnuviechadmin:appname') %}
{% set purpose = "for file server configuration management" %}
{% set mail_directory = salt['pillar.get']('gnuviechadmin:gvafile:mail_directory', '/home/mail') %}
{% set web_directory = salt['pillar.get']('gnuviechadmin:gvafile:web_directory', '/home/www') %}
{% set nfs_root = salt['pillar.get']('nfsserver:nfsroot', '/srv/nfs4') %}
{% set sftp_chroot = salt['pillar.get']('gnuviechadmin:gvafile:sftp_chroot', '/srv/sftp') %}
{% from 'gnuviechadmin/gvaapp_macros.sls' import create_celery_worker with context %}
include:
  - base
  - python.poetry
  - nfsserver

{{ mail_directory }}:
  file.directory:
    - user: root
    - group: root
    - mode: 0751

{{ web_directory }}:
  file.directory:
    - user: root
    - group: root
    - mode: 0751

{{ sftp_chroot }}:
  file.directory:
    - user: root
    - group: root
    - mode: 0755

{{ sftp_chroot }}/home:
  file.directory:
    - user: root
    - group: root
    - mode: 0751
    - require:
      - file: {{ sftp_chroot }}

bind_mount_nfs_mail:
  mount.fstab_present:
    - name: {{ mail_directory }}
    - fs_file: {{ nfs_root }}/mail
    - fs_vfstype: none
    - fs_mntops: bind
    - require:
      - file: {{ mail_directory }}
      - file: {{ nfs_root }}/mail
    - watch_in:
      - service: nfs-kernel-server

bind_mount_nfs_web:
  mount.fstab_present:
    - name: {{ web_directory }}
    - fs_file: {{ nfs_root }}/web
    - fs_vfstype: none
    - fs_mntops: bind
    - require:
      - file: {{ web_directory }}
      - file: {{ nfs_root }}/web
    - watch_in:
      - service: nfs-kernel-server

bind_mount_sftp_chroot:
  mount.fstab_present:
    - name: {{ web_directory }}
    - fs_file: {{ sftp_chroot }}/home
    - fs_vfstype: none
    - fs_mntops: bind
    - require:
      - file: {{ web_directory }}
      - file: {{ sftp_chroot }}/home

{{ create_celery_worker(gvaappname, purpose) }}

/etc/sudoers.d/{{ gvaappname }}:
  file.managed:
    - user: root
    - group: root
    - source: salt://gnuviechadmin/{{ gvaappname }}/sudoers
    - template: jinja
    - context:
        app_user: {{ salt['grains.get']('gnuviechadmin:user', gvaappname) }}
    - check_cmd: /usr/sbin/visudo -c -f
    - require:
      - pkg: sudo
