ldapserver-packages:
  debconf.set:
    - name: slapd
    - data:
        'slapd/domain': {'type': 'string', 'value': '{{ salt["pillar.get"]("gnuviechadmin:ldap_domain") }}'}
        'slapd/password1': {'type': 'string', 'value': '{{ salt["pillar.get"]("slapd:admin_password") }}'}
        'slapd/password2': {'type': 'string', 'value': '{{ salt["pillar.get"]("slapd:admin_password") }}'}
  pkg.installed:
    - pkgs:
      - ldap-utils
      - ldapscripts
      - ldapvi
      - slapd
  service.running:
    - name: slapd
    - require:
      - pkg: ldapserver-packages
      - debconf: slapd
