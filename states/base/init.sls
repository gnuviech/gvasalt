debian-repo:
  pkgrepo.managed:
    - humanname: Debian
    - name: deb http://deb.debian.org/debian/ {{ salt['grains.get']('oscodename', 'buster') }} main
    - file: /etc/apt/sources.list

debian-updates-repo:
  pkgrepo.managed:
    - humanname: Debian updates
    - name: deb http://deb.debian.org/debian/ {{ salt['grains.get']('oscodename', 'buster') }}-updates main
    - file: /etc/apt/sources.list

debian-security-repo:
  pkgrepo.managed:
    - humanname: Debian security
    - name: deb http://security.debian.org/ {{ salt['grains.get']('oscodename', 'buster') }}/updates main
    - file: /etc/apt/sources.list

httpredir-debian-repo:
  pkgrepo.absent:
    - name: deb http://httpredir.debian.org/debian {{ salt['grains.get']('oscodename', 'buster') }} main
    - file: /etc/apt/sources.list

backports-repo:
  pkgrepo.managed:
    - humanname: Debian backports
    - name: deb http://deb.debian.org/debian/ {{ salt['grains.get']('oscodename', 'buster') }}-backports main
    - file: /etc/apt/sources.list.d/backports.list

salt-repo:
  pkgrepo.managed:
    - humanname: Saltstack Repository
    - name: deb https://repo.saltstack.com/py3/debian/{{ salt['grains.get']('osmajorrelease') }}/amd64/latest {{ salt['grains.get']('oscodename', 'buster') }} main
    - dist: {{ salt['grains.get']('oscodename', 'buster') }}
    - file: /etc/apt/sources.list.d/saltstack.list
    - key_url: https://repo.saltstack.com/py3/debian/{{ salt['grains.get']('osmajorrelease') }}/amd64/latest/SALTSTACK-GPG-KEY.pub

/etc/apt/apt.conf.d/02norecommends:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - contents: 'Apt::Install-Recommends "false";'

/etc/apt/apt.conf.d/03translations:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - contents: 'Acquire::Languages "none";'

/etc/apt/apt.conf.d/04compression:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - contents: 'Acquire::CompressionTypes::Order {"xz"; "gz"; "bz2"; "uncompressed"};'

base:
  pkg.installed:
    - pkgs:
      - apt-transport-https
      - bash-completion
      - bsdmainutils
      - ca-certificates
      - debconf-utils
      - etckeeper
      - git
      - less
      - locales-all
      - lsb-release
      - tmux
      - virt-what

sudo:
  pkg.installed

/etc/sudoers.d/sudonopasswd:
  file:
    - managed
    - mode: 0440
    - user: root
    - group: root
    - source: salt://base/sudonopasswd
    - require:
      - pkg: sudo

/etc/salt/grains:
  file.managed:
    - user: root
    - group: root
    - mode: 0600
    - replace: False

nano:
  pkg.purged

update-system:
  pkg.uptodate:
    - refresh: True
