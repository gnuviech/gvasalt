include:
  - uwsgi

uwsgi-plugin-python3:
  pkg.installed:
    - require_in:
      - service: uwsgi
