---
uwsgi:
  pkg:
    - installed
  service.running:
    - enable: true
    - reload: false
    - require:
      - pkg: uwsgi
