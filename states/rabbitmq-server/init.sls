rabbitmq-server:
  pkg:
    - installed
  service:
    - running
    - requires:
      - pkg: rabbitmq-server

guest:
  rabbitmq_user:
    - absent

rabbitmq_management:
  rabbitmq_plugin:
    - enabled
    - watch_in:
      - service: rabbitmq-server
