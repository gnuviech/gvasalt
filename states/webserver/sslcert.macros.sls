{%- macro key_cert(domain_name) %}
{% set nginx_ssl_keydir = salt['pillar.get']('nginx:sslkeydir', '/etc/nginx/ssl/private') %}
{% set nginx_ssl_certdir = salt['pillar.get']('nginx:sslcertdir', '/etc/nginx/ssl/certs') %}
{% set keyfile = nginx_ssl_keydir + '/' + domain_name + '.key.pem' %}
{% set certfile = nginx_ssl_certdir + '/' + domain_name + '.crt.pem' %}

{{ keyfile }}:
  rsa_key.valid_key:
    - bits: {{ salt['pillar.get']('nginx:keylength:' + domain_name, 2048) }}
    - require:
      - file: {{ nginx_ssl_keydir }}
      - pkg: python3-cryptography
    - require_in:
      - file: /etc/nginx/sites-available/{{ domain_name }}
      - service: nginx

{{ certfile }}:
  cmd.run:
    - name: openssl req -new -x509 -key {{ keyfile }} -subj '/CN={{ domain_name }}' -days 730 -out {{ certfile }}
    - require:
      - rsa_key: {{ keyfile }}
    - creates: {{ certfile }}
  x509_certificate.valid_certificate:
    - require:
      - file: {{ nginx_ssl_certdir }}
      - cmd: {{ certfile }}
      - pkg: python3-cryptography
    - require_in:
      - file: /etc/nginx/sites-available/{{ domain_name }}
      - service: nginx
{% endmacro %}
