vim-nox:
  pkg.installed

editor:
  alternatives.set:
    - path: /usr/bin/vim.nox
    - require:
      - pkg: vim-nox
