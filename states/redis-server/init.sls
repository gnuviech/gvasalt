redis-server:
  pkg:
    - installed
  service.running:
    - enable: true
    - require:
      - pkg: redis-server

python-redis:
  pkg.installed:
    - reload_modules: true

/etc/redis/redis.conf:
  file.managed:
    - source: salt://redis-server/redis.conf
    - template: jinja
    - user: root
    - group: root
    - mode: 0644
    - watch_in:
      - service: redis-server
