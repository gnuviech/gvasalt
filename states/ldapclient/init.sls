ldapclient:
  debconf.set:
    - name: nslcd
    - data:
        'nslcd/ldap-base': {'type': 'string', 'value': '{{ pillar.get("ldap_base") }}'}
        'nslcd/ldap-uris': {'type': 'string', 'value': '{{ pillar.get("ldap_uris") }}'}
  pkg.installed:
    - pkgs:
      - ldap-utils
      - nslcd
      - libnss-ldapd
{% if 'ldap_auth' in pillar %}
      - libpam-ldapd
{% endif %}
  service.running:
    - name: nslcd
    - require:
      - pkg: ldapclient
      - debconf: nslcd

libnss-ldapd-reconfigure:
  cmd.wait:
    - name: dpkg-reconfigure --frontend=noninteractive libnss-ldapd
    - require:
      - pkg: ldapclient
    - watch:
      - debconf: libnss-ldapd-debconf

libnss-ldapd-debconf:
  debconf.set:
    - name: libnss-ldapd
    - data:
        'libnss-ldapd/nsswitch': {'type': 'multiselect', 'value': 'group, passwd'}
