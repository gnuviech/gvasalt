{% set nfs_root = salt['pillar.get']('nfsserver:nfsroot', '/srv/nfs4') -%}

nfs-kernel-server:
  pkg:
    - installed
  service:
    - running
    - enable: True
    - require:
      - pkg: nfs-kernel-server

{{ nfs_root }}:
  file.directory:
    - user: root
    - group: root
    - mode: 0755

{{ nfs_root }}/mail:
  file.directory:
    - user: root
    - group: root
    - mode: 0751
    - require:
      - file: {{ nfs_root }}

{{ nfs_root }}/web:
  file.directory:
    - user: root
    - group: root
    - mode: 0751
    - require:
      - file: {{ nfs_root }}

acl:
    pkg.installed

/etc/exports:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - template: jinja
    - context:
        nfs_root: {{ nfs_root }}
    - source: salt://nfsserver/exports
    - watch_in:
      - service: nfs-kernel-server
