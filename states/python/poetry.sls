---
curl:
  pkg.installed

python3:
  pkg.installed

poetry-preconditions:
  pkg.installed:
    - pkgs:
      - python3-wheel
      - python3-pip
      - python3-setuptools

install_poetry:
  cmd.run:
    - name: curl -sSL https://install.python-poetry.org | POETRY_VERSION={{ salt['grains.get']('python:poetry:version', '1.4.2') }} POETRY_HOME=/usr/local/poetry /usr/bin/python3 -
    - creates: /usr/local/poetry/bin/poetry
    - require:
      - id: poetry-preconditions
      - pkg: curl
      - pkg: python3
